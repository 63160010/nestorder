import { IsInt, IsNotEmpty, Length, Min } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  price: number;
}
