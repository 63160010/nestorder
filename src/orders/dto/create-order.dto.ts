import { ArrayNotEmpty, IsInt, IsNotEmpty } from 'class-validator';
class CreatedOrderItemDao {
  @IsNotEmpty()
  @IsInt()
  productId: number;
  @IsNotEmpty()
  @IsInt()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  @IsInt()
  customerId: number;

  @ArrayNotEmpty()
  orderItems: CreatedOrderItemDao[];
}
