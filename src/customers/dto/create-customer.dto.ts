import { IsInt, IsNotEmpty, Length, Min } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  @IsInt()
  @Min(1)
  age: number;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  @Length(1)
  gender: string;
}
